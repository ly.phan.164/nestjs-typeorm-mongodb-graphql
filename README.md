# README

## Project Structure

```text
.
├── dist
├── src                       # @src/*
    ├── constants             # @constants
    ├── entities              # @entities
        ├── index.ts
    ├── interceptors          # @interceptors
        ├── ...
        ├── index.ts
    ├── modules               # @modules
        ├── ...
        ├── graphql           # @graphql
            ├── index.ts
        ├── index.ts
    ├── utils                 # @utils
        ├── ...
        ├── index.ts
    ├── app.module.ts
    ├── env.config.ts
    ├── graphql.schema.ts
    ├── main.ts
├── .env
├── paths.json
├── webpack.config.js
```

## Scripts

- Pre-commit checking included.

### Development

- Generate Graphql-schema definitions

```bash
yarn generate:schema
```

- Normal start

```bash
yarn start
```

- With [Nodemon](https://github.com/remy/nodemon)

```bash
yarn start:dev
```

- With [WebpackHMR](https://webpack.js.org/concepts/hot-module-replacement/): Please open 2 terminals to use this feature

```bash
yarn webpack
```

```bash
yarn start:hot
```

- Format code

```bash
yarn lint
```

## Production

- Build

```bash
yarn build
```

- Run

```bash
yarn start:prod
```
