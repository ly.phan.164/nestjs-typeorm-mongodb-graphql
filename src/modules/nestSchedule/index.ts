import { Module } from '@nestjs/common'
import { ScheduleModule } from 'nest-schedule'

@Module({
  imports: [ScheduleModule.register()]
})
export class NestScheduleModule { }
