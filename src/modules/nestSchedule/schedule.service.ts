import { Injectable } from '@nestjs/common'
import { Interval, Timeout, Cron } from 'nest-schedule'

import { ResolverClass, Console } from '@utils'

@Injectable()
export class ScheduleService extends ResolverClass {
  @Interval(1000 * 10, { key: `I${+new Date()}` })
  interval() {
    Console.log('Executing interval job')
  }

  @Timeout(1000 * Math.PI, { key: `T${+new Date()}` })
  async timeout() {
    Console.log('Executing timeout job')
  }

  @Cron('*/1 * * * *', { key: `C${+new Date()}`, waiting: true })
  cron() {
    Console.log('Executing cron job')
  }
}
