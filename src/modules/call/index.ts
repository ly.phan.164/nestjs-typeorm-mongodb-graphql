import { Module } from '@nestjs/common'

import { CallResolvers } from './call.resolver'

@Module({ providers: [CallResolvers] })
export class CallModule {}
