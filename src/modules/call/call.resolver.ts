import { Resolver, Args, Context, Query, Mutation, Subscription } from '@nestjs/graphql'
import { UseInterceptors } from '@nestjs/common'

import { Logging } from '@interceptors'
import { IError } from '@utils'
import { _appConfigs, _code, _statusCall } from '@constants'

import { CallInput, OnNotificationResponse, Call as CallGraphql } from '@src/graphql.schema'
import { Call, User, dbTableName } from '@src/entities'
import { IContext, INotiPayload, SContext } from '@type'
import { AbstractClass } from '../abstractClass'

@Resolver('Call')
@UseInterceptors(Logging)
export class CallResolvers extends AbstractClass {
  @Query('call')
  async call(
    @Context() context: IContext,
    @Args('callId') callId: string
  ): Promise<CallGraphql | IError> {
    try {
      if (!context.currentUserId) return this.err.Authentication()

      const [existedCall] = await this.mongoManager
        .aggregate(Call, [
          { $match: { _id: callId } },
          {
            $lookup: {
              from: dbTableName.USER,
              let: { userId: '$createdBy' },
              pipeline: [{ $match: { $expr: { $eq: ['$_id', '$$userId'] } } }],
              as: 'createdBy'
            }
          },
          { $unwind: { path: '$createdBy', preserveNullAndEmptyArrays: true } },
          {
            $lookup: {
              from: dbTableName.USER,
              let: { userId: '$receiver' },
              pipeline: [{ $match: { $expr: { $eq: ['$_id', '$$userId'] } } }],
              as: 'receiver'
            }
          },
          { $unwind: { path: '$receiver', preserveNullAndEmptyArrays: true } }
        ]).toArray()

      if (!existedCall) {
        return this.err.UserInput(_code.CALL_NOT_FOUND)
      }

      return existedCall
    } catch (err) {
      return this.catchErrors(err)
    }
  }

  @Query('callByUser')
  async callByUser(
    @Context() context: IContext,
    @Args('userId') userId: string,
    @Args('first') first: number = 10,
    @Args('offset') offset: number = 0
  ): Promise<Array<Call> | IError> {
    try {
      if (!context.currentUserId) return this.err.Authentication()

      return await this.mongoManager.aggregate(Call, [
        {
          $match: {
            $and: [
              {
                $or: [
                  {
                    $and: [{ createdBy: context.currentUserId }, { receiver: userId }]
                  },
                  {
                    $and: [{ createdBy: userId }, { receiver: context.currentUserId }]
                  }
                ]
              },
              {
                status: { $ne: _statusCall.NOT_COMPLETED }
              },
              {
                status: { $ne: _statusCall.CALLING }
              }
            ]
          }
        },
        { $sort: { createdAt: -1 } },
        { $skip: offset },
        { $limit: first },
        { $sort: { createdAt: 1 } },
        {
          $lookup: {
            from: dbTableName.USER,
            localField: 'createdBy',
            foreignField: '_id',
            as: 'createdBy'
          }
        },
        {
          $unwind: {
            path: '$createdBy',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $lookup: {
            from: dbTableName.USER,
            localField: 'receiver',
            foreignField: '_id',
            as: 'receiver'
          }
        },
        {
          $unwind: {
            path: '$receiver',
            preserveNullAndEmptyArrays: true
          }
        }

      ]).toArray()
    } catch (err) {
      return this.catchErrors(err)
    }
  }

  @Mutation('makeCall')
  async makeCall(
    @Context() context: IContext,
    @Args('callInput') callInput: CallInput
  ): Promise<string | IError> {
    try {
      const existedUser = await this.mongoManager.findOne(User, {
        _id: context.currentUserId,
        isActive: true
      })

      if (!existedUser) return this.err.Authentication()

      const receiver = await this.mongoManager.findOne(User, {
        _id: callInput.receiver,
        isActive: true
      })

      if (!receiver) return this.err.UserInput(_code.USER_NOT_FOUND)
      if (!receiver.isOnline) {
        await this.mongoManager.save(
          Call,
          new Call({
            createdBy: context.currentUserId,
            status: _statusCall.NOT_COMPLETED,
            receiver: callInput.receiver,
            isVideoCall: callInput.isVideoCall,
            isVoiceCall: callInput.isVoiceCall
          })
        )
        return this.err.UserInput(_code.USER_NOT_ONLINE)
      }

      const newCall = await this.mongoManager.save(
        Call,
        new Call({
          createdBy: context.currentUserId,
          status: _statusCall.CALLING,
          receiver: callInput.receiver,
          isVideoCall: callInput.isVideoCall,
          isVoiceCall: callInput.isVoiceCall
        })
      )

      if (!!newCall) {
        await this.publishNotification({
          type: this.PUBSUB._CALL,
          code: this.NOTIFICATION_CODE.INCOMING,
          data: JSON.stringify({ callId: newCall._id }),
          pubsubCreatedBy: context.currentUserId,
          receiver: callInput.receiver
        })
      }

      return newCall._id || ''
    } catch (err) {
      return this.catchErrors(err)
    }
  }

  @Mutation('acceptCall')
  async acceptCall(
    @Context() context: IContext,
    @Args('callId') callId: string
  ): Promise<boolean | IError> {
    try {
      const existedUser = await this.mongoManager.findOne(User, {
        _id: context.currentUserId,
        isActive: true
      })

      if (!existedUser) return this.err.Authentication()

      const existedCall = await this.mongoManager.findOne(Call, {
        _id: callId,
        status: _statusCall.CALLING
      })

      if (!existedCall) return this.err.UserInput(_code.CALL_NOT_FOUND)
      if (existedCall.receiver !== context.currentUserId) return this.err.Authentication()

      const updateCall = await this.mongoManager.updateOne(
        Call,
        {
          _id: callId
        },
        {
          $set: { status: _statusCall.ACCEPT, startedAt: Date.now() }
        }
      )

      if (!!updateCall) {
        await this.publishNotification({
          type: this.PUBSUB._CALL,
          code: this.NOTIFICATION_CODE.ACCEPT,
          data: '',
          pubsubCreatedBy: context.currentUserId,
          receiver: existedCall.createdBy
        })
      }

      return !!updateCall
    } catch (err) {
      return this.catchErrors(err)
    }
  }

  @Mutation('refuseCall')
  async refuseCall(
    @Context() context: IContext,
    @Args('callId') callId: string
  ): Promise<boolean | IError> {
    try {
      const existedUser = await this.mongoManager.findOne(User, {
        _id: context.currentUserId,
        isActive: true
      })

      if (!existedUser) return this.err.Authentication()

      const existedCall = await this.mongoManager.findOne(Call, {
        _id: callId,
        status: _statusCall.CALLING
      })

      if (!existedCall) return this.err.UserInput(_code.CALL_NOT_FOUND)
      if (existedCall.receiver !== context.currentUserId) return this.err.Authentication()

      const updateCall = await this.mongoManager.updateOne(
        Call,
        {
          _id: callId
        },
        {
          $set: { status: _statusCall.DENIED }
        }
      )

      if (!!updateCall) {
        await this.publishNotification({
          type: this.PUBSUB._CALL,
          code: this.NOTIFICATION_CODE.DENIED,
          data: '',
          pubsubCreatedBy: context.currentUserId,
          receiver: existedCall.createdBy
        })
      }

      return !!updateCall
    } catch (err) {
      return this.catchErrors(err)
    }
  }

  @Mutation('endCall')
  async endCall(
    @Context() context: IContext,
    @Args('callId') callId: string
  ): Promise<boolean | IError> {
    try {
      const existedUser = await this.mongoManager.findOne(User, {
        _id: context.currentUserId,
        isActive: true
      })

      if (!existedUser) return this.err.Authentication()

      const existedCall = await this.mongoManager.findOne(Call, {
        _id: callId,
        status: _statusCall.ACCEPT
      })

      if (!existedCall) return this.err.UserInput(_code.CALL_NOT_FOUND)

      const updateCall = await this.mongoManager.updateOne(
        Call,
        {
          _id: callId
        },
        {
          $set: { status: _statusCall.SUCCESS, endedAt: Date.now() }
        }
      )

      if (!!updateCall) {
        await this.publishNotification({
          type: this.PUBSUB._CALL,
          code: this.NOTIFICATION_CODE.SUCCESS,
          data: '',
          pubsubCreatedBy: context.currentUserId,
          receiver:
            existedCall.createdBy === context.currentUserId
              ? existedCall.receiver
              : context.currentUserId
        })
      }

      return !!updateCall
    } catch (err) {
      return this.catchErrors(err)
    }
  }

  @Mutation('cancelCall')
  async cancelCall(
    @Context() context: IContext,
    @Args('callId') callId: string
  ): Promise<boolean | IError> {
    try {
      const existedUser = await this.mongoManager.findOne(User, {
        _id: context.currentUserId,
        isActive: true
      })

      if (!existedUser) return this.err.Authentication()

      const existedCall = await this.mongoManager.findOne(Call, {
        _id: callId,
        status: _statusCall.CALLING
      })

      if (!existedCall) return this.err.UserInput(_code.CALL_NOT_FOUND)
      if (existedCall.createdBy !== context.currentUserId) return this.err.Authentication()

      const updateCall = await this.mongoManager.updateOne(
        Call,
        {
          _id: callId
        },
        {
          $set: { status: _statusCall.MISSING }
        }
      )

      if (!!updateCall) {
        await this.publishNotification({
          type: this.PUBSUB._CALL,
          code: this.NOTIFICATION_CODE.MISSING,
          data: '',
          pubsubCreatedBy: context.currentUserId,
          receiver: existedCall.receiver
        })
      }

      return !!updateCall
    } catch (err) {
      return this.catchErrors(err)
    }
  }

  @Subscription(() => String, {
    resolve: ({ callId, ...rest }: INotiPayload): OnNotificationResponse => rest,
    filter: (payload: INotiPayload, { }, { currentUserId }: SContext) => {

      return true
    }
  })
  onGlobalNotification() {
    return this.publisher.asyncIterator(this.PUBSUB._CALL)
  }
}
