import { Resolver, Args, Context, Query, Mutation } from '@nestjs/graphql'
import { UseInterceptors } from '@nestjs/common'

import { Logging } from '@interceptors'
import { IContext } from '@type'
import { IError, isStringsValid, comparePassword, hashPassword } from '@utils'
import { _appConfigs, _code } from '@constants'

import {
  LoginInput,
  LoginResponse,
  User as UserGQLType,
  NewUserInput,
  UpdateUserInput,
} from '@src/graphql.schema'
import { User, dbTableName } from '@src/entities'
import { tradeToken } from '@src/auth'
import { AbstractClass } from '../abstractClass'

@Resolver('User')
@UseInterceptors(Logging)
export class UserResolvers extends AbstractClass {
  @Mutation('login')
  async login(@Args('loginInput') args: LoginInput): Promise<LoginResponse | IError> {
    try {
      if (!isStringsValid([args.username, args.password])) {
        return this.err.UserInput('Username and Password can not be null or contain white spaces!')
      }

      const user = await this.mongoManager.findOne(User, {
        where: {
          username: args.username
        }
      })

      if (!user) {
        return this.err.UserInput(_code.USER_NOT_FOUND)
      }

      if (await comparePassword(args.password, user.password)) {
        return await tradeToken(user)
      }

      return this.err.Authentication(_code.USER_NOT_FOUND)
    } catch (error) {
      return this.err.Apollo(error)
    }
  }

  @Query('myInfo')
  async myInfo(@Context() context: IContext): Promise<UserGQLType | IError> {
    try {
      if (context.currentUserId) {
        const [existedUser] = await this.mongoManager
          .aggregate(User, [
            { $match: { _id: context.currentUserId } },
            {
              $lookup: {
                from: dbTableName.USER,
                let: { userId: '$createdBy' },
                pipeline: [{ $match: { $expr: { $eq: ['$_id', '$userId'] } } }],
                as: 'createdBy'
              }
            },
            { $unwind: { path: '$createdBy', preserveNullAndEmptyArrays: true } }
          ])
          .toArray()

        if (!existedUser) {
          return this.err.UserInput(_code.USER_NOT_FOUND)
        }

        return existedUser
      }

      return this.err.Authentication()
    } catch (err) {
      return this.catchErrors(err)
    }
  }

  @Query('user')
  async user(
    @Context() context: IContext,
    @Args('userId') userId?: string
  ): Promise<UserGQLType | IError> {
    try {
      if (context.currentUserId) {
        return
      }
      return this.err.Authentication()
    } catch (error) {
      return this.catchErrors(error)
    }
  }

  @Query('users')
  async users(@Context() context: IContext): Promise<UserGQLType[] | IError> {
    const users = await this.mongoManager
      .aggregate(User, [
        {
          $lookup: {
            from: dbTableName.USER,
            let: { userId: '$createdBy' },
            pipeline: [{ $match: { $expr: { $eq: ['$_id', '$userId'] } } }],
            as: 'createdBy'
          }
        },
        { $unwind: { path: '$createdBy', preserveNullAndEmptyArrays: true } }
      ])
      .toArray()
    return users
  }

  @Mutation('createUser')
  async createUser(
    @Context() context: IContext,
    @Args('newUser') newUser: NewUserInput
  ): Promise<boolean | IError> {
    try {
      if (context.currentUserId) {
        return
      }
      return this.err.Authentication()
    } catch (error) {
      return this.catchErrors(error)
    }
  }

  @Mutation('register')
  async register(@Args('newUser') newUser: NewUserInput): Promise<string | IError> {
    try {
      const existedUser = await this.mongoManager.findOne(User, {
        username: newUser.username,
        isActive: true
      })

      if (existedUser) {
        return this.err.UserInput(_code.ALREADY_USERNAME)
      }

      const user = await this.mongoManager.save(
        User,
        new User({
          ...newUser,
          password: await hashPassword(newUser.password)
        })
      )

      return !!user ? user._id : ''
    } catch (err) {
      return this.catchErrors(err)
    }
  }

  @Mutation('updateUser')
  async updateUser(
    @Args('userId') userId: string,
    @Args('update') update: UpdateUserInput
  ): Promise<boolean | IError> {
    try {
      const existedUser = await this.mongoManager.findOne(User, { _id: userId })

      if (!existedUser) {
        return this.err.UserInput(_code.USER_NOT_FOUND)
      }

      const updateResult = await this.mongoManager.updateOne(
        User,
        { _id: userId },
        { $set: { ...update } }
      )

      return !!updateResult
    } catch (error) {
      return this.catchErrors(error)
    }
  }

  @Mutation('deleteUser')
  async deleteUser(
    @Context() context: IContext,
    @Args('userIds') userIds: string[]
  ): Promise<string[] | IError> {
    try {
      if (context.currentUserId) {
        return
      }
      return this.err.Authentication()
    } catch (error) {
      return this.catchErrors(error)
    }
  }
}
