import { Module, Logger } from '@nestjs/common'
import { getMongoManager, getMongoRepository } from 'typeorm'
import { GraphQLModule } from '@nestjs/graphql'
import * as depthLimit from 'graphql-depth-limit'

import { _appConfigs, _statusUser } from '@constants'
import { extractIdFromToken, Console } from '@utils'
import { User } from '@entities'

import { DeprecatedDirective } from './directives'
import { generatorConfigs } from './settings'
import { SContext, IContext } from '@type'

const { GRAPHQL_PATH, GRAPHQL_DEPTH_LIMIT, PRODUCTION, TOKEN } = _appConfigs

@Module({
  imports: [
    GraphQLModule.forRootAsync({
      useFactory: () => ({
        // NOTE: Load graphql files
        path: GRAPHQL_PATH,
        typePaths: ['./**/*.graphql'],

        // NOTE: Subscriptions
        installSubscriptionHandlers: true,
        subscriptions: {
          path: GRAPHQL_PATH,
          onConnect: async connectionParams => {
            process.env.NODE_ENV !== PRODUCTION &&
              Logger.debug(`🔗  Connected to websocket`, 'GraphQL')

            if (connectionParams[TOKEN]) {
              const currentUserId = extractIdFromToken(connectionParams[TOKEN])
              if (currentUserId) {
                const currentUser = await getMongoManager().findOne(User, {
                  where: { _id: currentUserId },
                  select: ['_id']
                })
                if (currentUser) {
                  await getMongoRepository(User).updateOne(
                    { _id: currentUserId },
                    {
                      $set: { isOnline: true }
                    }
                  )
                  return { currentUserId }
                }
              } else {
                throw new Error('Missing or invalid auth token!')
              }
            }
            return null
          },
          onDisconnect: async (websocket, context) => {
            const { initPromise } = context
            const { currentUserId } = await initPromise
            process.env.NODE_ENV !== PRODUCTION &&
              Logger.error(`❌  Disconnected to websocket`, '', 'GraphQL', false)

            await getMongoRepository(User).updateOne(
              { _id: currentUserId },
              {
                $set: { isOnline: false }
              }
            )
          }
        },

        // NOTE: Graphql schema definitions generator
        ...((process.env.GENERATE_SCHEMA || process.env.NODE_ENV === PRODUCTION) && {
          definitions: { ...generatorConfigs }
        }),

        // NOTE: Schema directives
        schemaDirectives: {
          deprecated: DeprecatedDirective
        },

        // NOTE: Upload
        uploads: {
          maxFileSize: 10000000, // 10 MB
          maxFiles: 5
        },

        // NOTE: Graphql context
        context: async ({ req, connection }): Promise<SContext | IContext> => {
          // For websocket/subscriptions
          if (connection) {
            if (connection.context.currentUserId) {
              if (connection.variables) {
                return {
                  connection,
                  currentUserId: connection.context.currentUserId,
                  variables: connection.variables
                }
              }
              return {
                connection,
                currentUserId: connection.context.currentUserId
              }
            }
            return { connection }
          } else {
            // For queries/mutations
            if (req.headers[TOKEN]) {
              const currentUserId = extractIdFromToken(req.headers[TOKEN])
              if (currentUserId) {
                if (
                  !!(await getMongoManager().findOne(User, {
                    where: { _id: currentUserId },
                    select: ['_id']
                  }))
                ) {
                  return { req, currentUserId }
                }
              }
            }

            return { req }
          }
        },

        // NOTE: Stop users from making high depth queries/mutations
        validationRules: [
          depthLimit(GRAPHQL_DEPTH_LIMIT, {}, info => {
            const [[name, depth]] = Object.entries(info)
            const message = `Queries/Mutations depth reached level ${depth} on ${name ||
              '"unknown"'}`
            if (depth === GRAPHQL_DEPTH_LIMIT - 1) {
              Console.warn(message)
            } else if (depth === GRAPHQL_DEPTH_LIMIT) {
              Console.error(message)
            }
          })
        ],

        // NOTE: Playground
        introspection: true,
        playground:
          process.env.NODE_ENV === PRODUCTION
            ? false
            : {
              settings: {
                'general.betaUpdates': false,
                'editor.cursorShape': 'line',
                'editor.fontSize': 14,
                'editor.fontFamily': `'Source Code Pro', 'Consolas', 'Droid Sans Mono', 'Monaco', monospace`,
                'editor.theme': 'dark',
                'editor.reuseHeaders': true,
                'request.credentials': 'omit',
                'tracing.hideTracingResponse': false
              }
            },
        formatError: ({ message, locations, path, extensions: { code } }) => {
          return {
            message: message.toString().replace(/(Error: |Authentication|UserInputError: )+/g, ''),
            locations,
            path,
            code
          }
        }
      })
    })
  ]
})
export class GraphqlModule { }
