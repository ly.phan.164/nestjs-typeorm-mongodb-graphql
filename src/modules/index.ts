// Graphql
import { GraphqlModule } from './graphql'

// TypeORM
import { TypemORMModule } from './typeORM'

// Schedule/CronJob
import { NestScheduleModule } from './nestSchedule'

// Others
import { UserModule } from './user'
import { CallModule } from './call'

export default [GraphqlModule, NestScheduleModule, TypemORMModule, UserModule, CallModule]
