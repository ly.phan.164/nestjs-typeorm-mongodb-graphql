import { Module } from '@nestjs/common'
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm'

import envConfig from '@src/env.config'

@Module({
  imports: [TypeOrmModule.forRoot(envConfig.ormConfig as TypeOrmModuleOptions)]
})
export class TypemORMModule {}
