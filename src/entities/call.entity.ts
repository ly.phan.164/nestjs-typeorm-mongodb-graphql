import { Entity, Column, ObjectIdColumn } from 'typeorm'

import { idGenerator } from '@utils'
import { dbTableName } from '.'

@Entity({ name: dbTableName.CALL, orderBy: { _id: 'ASC' } })
export class Call {
  @ObjectIdColumn()
  _id?: string

  @Column()
  createdAt?: number

  @Column()
  createdBy?: string

  @Column()
  status?: string

  @Column()
  receiver?: string

  @Column()
  startedAt?: number

  @Column()
  endedAt?: number

  @Column()
  isVideoCall: boolean

  @Column()
  isVoiceCall: boolean

  @Column()
  isRecord?: boolean

  constructor(args: Partial<Call>) {
    type T = Partial<Call>
    Object.assign<T, T, T>(
      this,
      {
        _id: idGenerator.mainId(),
        createdAt: +new Date()
      },
      args
    )
  }
}
