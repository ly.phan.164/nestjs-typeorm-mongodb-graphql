export const dbTableName = {
  USER: 'users',
  CALL: 'calls'
}

export * from './user.entity'
export * from './call.entity'
