import { Entity, Column, ObjectIdColumn } from 'typeorm'

import { idGenerator } from '@utils'
import { dbTableName } from '.'

@Entity({ name: dbTableName.USER, orderBy: { _id: 'ASC' } })
export class User {
  @ObjectIdColumn()
  _id?: string

  @Column()
  createdAt?: number

  @Column()
  createdBy?: string

  @Column()
  email?: string

  @Column()
  phone?: string

  @Column()
  firstname?: string

  @Column()
  isActive?: boolean

  @Column()
  isLocked?: boolean

  @Column()
  imageUrl?: string

  @Column()
  lastname?: string

  @Column()
  password: string

  @Column()
  username: string

  @Column()
  isOnline: boolean

  constructor(args: Partial<User>) {
    type T = Partial<User>
    Object.assign<T, T, T>(
      this,
      {
        _id: idGenerator.mainId(),
        createdAt: +new Date(),
        isActive: true,
        isLocked: false,
        isOnline: false
      },
      args
    )
  }
}
