import { _appConfigs } from '@constants'
import { TypeOrmModuleOptions } from '@nestjs/typeorm'

const { DEVELOPMENT } = _appConfigs

const {
  NODE_ENV,
} = process.env

const config = {
  development: {
    // cors: true,
    ormConfig: {
      type: 'mongodb',
      url: 'mongodb+srv://kaonashi:1vrZrq3H7jxr7gTn@cluster0-pvh5w.mongodb.net/test?retryWrites=true&w=majority',
      useNewUrlParser: true,
      useUnifiedTopology: true,
      synchronize: true,
      keepConnectionAlive: true,
      entities: [
        ...(process.env.HOT
          ? [
            ...require
              .context('.', true, /\.entity\.ts$/)
              .keys()
              .map(id => Object.values(require.context('.', true, /\.entity\.ts$/)(id))[0])
          ]
          : ['src/**/**.entity{.ts,.js}'])
      ]
    } as TypeOrmModuleOptions
  },
  production: {
    // cors: false,
    ormConfig: {
      type: 'mongodb',
      url: `mongodb://${process.env.MONGO_URL}`,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      synchronize: false,
      entities: ['dist/**/**.entity{.ts,.js}']
    } as TypeOrmModuleOptions
  }
}

export default config[NODE_ENV || DEVELOPMENT]
