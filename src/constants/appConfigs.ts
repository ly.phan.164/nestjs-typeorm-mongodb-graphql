// *: Backend
const DEFAULT_PORT: number = 3000 // edit
const DEFAULT_SECRET_KEY = 'kaonashi'

// *: Mongodb
const DEFAULT_MONGODB_PORT: number = 13279 // edit
const DEFAULT_MONGODB_NAME: string = 'newapp' // edit

// *: Graphql
const GRAPHQL_DEPTH_LIMIT: number = 3
const GRAPHQL_PATH: string = '/graphql' // edit
const SCHEMA_PATH: string = '/schema' // edit

// *: jsonwebtoken
const ISSUER: string = 'Kaonashi corp'
const AUDIENCE: string = 'http://kaonashi164.github.io'
const ACCESS_TOKEN: string = 'access-token'
const ACCESS_TOKEN_SECRET: string = 'access-token-key'
const REFRESH_TOKEN: string = 'refresh-token'
const REFRESH_TOKEN_SECRET: string = 'refresh-token-key'
const EMAIL_TOKEN: string = 'email-token'
const EMAIL_TOKEN_SECRET: string = 'email-token-key'
const RESETPASS_TOKEN: string = 'resetpass-token'
const RESETPASS_TOKEN_SECRET: string = 'resetpass-token-key'

// *: Mode
const DEVELOPMENT: string = 'development'
const PRODUCTION: string = 'production'

// *: Connection var
const TOKEN: string = 'access-token'

// *: BCrypt
const SALT_ROUNDS: number = 12

// *: LRU cache
const CACHE_AMOUNT = 1000

export const _appConfigs = {
  DEFAULT_PORT,
  DEFAULT_MONGODB_PORT,
  DEFAULT_MONGODB_NAME,
  DEFAULT_SECRET_KEY,
  GRAPHQL_DEPTH_LIMIT,
  GRAPHQL_PATH,
  SCHEMA_PATH,
  DEVELOPMENT,
  PRODUCTION,
  TOKEN,
  SALT_ROUNDS,
  CACHE_AMOUNT,
  ISSUER,
  AUDIENCE,
  ACCESS_TOKEN,
  ACCESS_TOKEN_SECRET,
  REFRESH_TOKEN,
  REFRESH_TOKEN_SECRET,
  EMAIL_TOKEN,
  EMAIL_TOKEN_SECRET,
  RESETPASS_TOKEN,
  RESETPASS_TOKEN_SECRET
}
