
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class CallInput {
    receiver?: string;
    isVideoCall?: boolean;
    isVoiceCall?: boolean;
}

export class LoginInput {
    username?: string;
    password?: string;
}

export class NewUserInput {
    username: string;
    password: string;
    firstname?: string;
    lastname?: string;
    email?: string;
    phone?: string;
}

export class UpdateUserInput {
    password?: string;
    firstname?: string;
    lastname?: string;
    email?: string;
}

export class Call {
    _id?: string;
    createdAt?: number;
    createdBy?: User;
    status?: string;
    receiver?: User;
    startedAt?: number;
    endedAt?: number;
    isVideoCall?: boolean;
    isVoiceCall?: boolean;
    isRecord?: boolean;
}

export class LoginResponse {
    token?: string;
}

export abstract class IMutation {
    abstract makeCall(callInput: CallInput): string | Promise<string>;

    abstract acceptCall(callId: string): boolean | Promise<boolean>;

    abstract refuseCall(callId: string): boolean | Promise<boolean>;

    abstract endCall(callId: string): boolean | Promise<boolean>;

    abstract cancelCall(callId: string): boolean | Promise<boolean>;

    abstract login(loginInput: LoginInput): LoginResponse | Promise<LoginResponse>;

    abstract createUser(newUser: NewUserInput): boolean | Promise<boolean>;

    abstract register(newUser: NewUserInput): string | Promise<string>;

    abstract updateUser(userId: string, update: UpdateUserInput): boolean | Promise<boolean>;

    abstract deleteUser(userIds?: string[]): string[] | Promise<string[]>;
}

export class OnNotificationResponse {
    pubsubCreatedBy?: string;
    code?: string;
    data?: string;
    receiver?: string;
}

export abstract class IQuery {
    abstract call(callId?: string): Call | Promise<Call>;

    abstract calls(): Call[] | Promise<Call[]>;

    abstract callByUser(userId: string, first?: number, offset?: number): Call[] | Promise<Call[]>;

    abstract user(userId?: string): User | Promise<User>;

    abstract users(): User[] | Promise<User[]>;

    abstract myInfo(): User | Promise<User>;
}

export abstract class ISubscription {
    abstract onGlobalNotification(): OnNotificationResponse | Promise<OnNotificationResponse>;
}

export class User {
    _id?: string;
    username?: string;
    imageUrl?: string;
    firstname?: string;
    lastname?: string;
    email?: string;
    isLocked?: boolean;
    createdAt?: number;
    createdBy?: User;
    phone?: string;
    isOnline?: boolean;
}
