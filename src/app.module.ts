import { Module } from '@nestjs/common'

import Modules from './modules'

@Module({ imports: Array.from(Modules) })
export class ApplicationModule {}
