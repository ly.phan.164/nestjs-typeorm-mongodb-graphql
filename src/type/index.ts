/**
 * Subscriptions Context
 * @readonly `connection`: any
 * @readonly `currentUserId?`: string
 * @readonly `variables?`: any
 */
export declare interface SContext {
  connection: any
  currentUserId?: string
  variables?: any
}

/**
 * Queries / Mutations Context
 * @readonly `req`: any
 * @readonly `currentUserId?`: string
 */
export declare interface IContext {
  req: any
  currentUserId?: string
}

export interface INotiPayload {
  callId: string
  receiver: string
  code: string
  pubsubCreatedBy: string
}
