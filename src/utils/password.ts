import { hash, compare } from 'bcrypt'

import { _appConfigs } from '@constants'

export const hashPassword = async (password: string): Promise<string> => {
  return await hash(password, _appConfigs.SALT_ROUNDS)
}

export const comparePassword = async (password: string, hashString: string): Promise<boolean> => {
  return await compare(password, hashString)
}
